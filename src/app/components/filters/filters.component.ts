import {Component, Output, EventEmitter} from '@angular/core';
import {FiltersModel, ProductsStatus} from '../../models';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent {

  productsStatus = ProductsStatus;
  @Output() filtersChange = new EventEmitter<FiltersModel>();
  private model: FiltersModel = {};

  constructor() { }

  onStatusChange(event: any) {
    this.model = {status: event.target.value};
    this.filtersChange.emit(this.model);
  }
}


