import {Component, Input} from '@angular/core';
import {ProductModel} from '../../models';

@Component({
  selector: 'app-product-tile',
  templateUrl: './product-tile.component.html',
  styleUrls: ['./product-tile.component.scss']
})
export class ProductTileComponent {

  @Input() product: ProductModel = {name: '', statuses: [], price: 0};
}
