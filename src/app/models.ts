export enum ProductsStatus {
  Recommended = 'recommended',
  Saleout = 'saleout',
  Bestseller = 'bestseller',
  Promotion = 'promotion',
  New = 'new'
}

export interface ProductModel {
  name: string;
  statuses: ProductsStatus[];
  price: number;

}

export interface FiltersModel {
  status?: ProductsStatus
}
