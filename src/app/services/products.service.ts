import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProductModel} from '../models';
import { map } from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getProducts():Observable<ProductModel[]> {
    return this.httpClient.get("./assets/products.json").pipe(map( res =>
      Object.values(res)
      .filter(y => typeof y === 'object')
      .map(x => ({
        name: x.prod_name,
        statuses: x.prod_status?.split(',').filter((x: string) => !!x) || [],
        price: x.prod_price
      }))
    ));
  }
}
