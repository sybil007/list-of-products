import {DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {CommonModule, registerLocaleData} from '@angular/common';
import localePL from '@angular/common/locales/pl';
import {FiltersComponent} from './components/filters/filters.component';
import { ProductTileComponent } from './components/product-tile/product-tile.component';
import { FooterComponent } from './components/footer/footer.component';

registerLocaleData(localePL);

@NgModule({
  declarations: [
    AppComponent,
    FiltersComponent,
    ProductTileComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    CommonModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pl' },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'PLN' },
  ]
  ,
  bootstrap: [AppComponent]
})
export class AppModule { }
