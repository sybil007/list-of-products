import {Component, OnInit} from '@angular/core';
import {FiltersModel, ProductModel} from './models';
import {ProductsService} from './services/products.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  allProductsList: ProductModel[] = [];
  filteredProductsList: ProductModel[] = [];

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.getProductList();
  }

  private getProductList() {
    this.productsService.getProducts().subscribe((res: ProductModel[]) => {
      this.allProductsList = res;
      this.filteredProductsList = this.allProductsList;
    });
  }

  public filterProducts (event: FiltersModel) {
    this.filteredProductsList = this.allProductsList;

    if (event.status) {
      this.filteredProductsList = this.filteredProductsList.filter(x => x.statuses
      .find(status => status === event.status));
    }
  }
}
